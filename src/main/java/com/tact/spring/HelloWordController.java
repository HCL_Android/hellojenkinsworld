package com.tact.spring;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWordController {
	
	
	@GetMapping("/home1") 
	public String hello()
	{
		return "Hello HCL Word v1";
	}
	
	@GetMapping("/home2") 
	public String hello2()
	{
		return "Hello HCL Wod v2";
	}
	
	@GetMapping("/home4") 
	public String hello4()
	{
		return "Hello HCL Word v4";
	}
	
	@GetMapping("/home5") 
	public String home5()
	{
		return "Hello HCL Word v5";
	}
	
}
